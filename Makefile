PREFIX ?= /usr/local
VERSION = "v0.0.1"

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/gmx-wrapper.sh $(DESTDIR)$(PREFIX)/bin/gmx
	install -m 0755 scripts/gmx-select.sh $(DESTDIR)$(PREFIX)/bin/gmx-select

install-cuda:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/gmx-cuda-wrapper.sh $(DESTDIR)$(PREFIX)/bin/gmx
	install -m 0755 scripts/gmx-cuda-select.sh $(DESTDIR)$(PREFIX)/bin/gmx-select

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/gmx
	@$(RM) $(DESTDIR)$(PREFIX)/bin/gmx-select
	@docker rmi claughton/gromacs2018:$(VERSION)
	@docker rmi claughton/gomacs2018:latest

uninstall-cuda:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/gmx
	@$(RM) $(DESTDIR)$(PREFIX)/bin/gmx-select
	@docker rmi claughton/gromacs2018-cuda:$(VERSION)
	@docker rmi claughton/gomacs2018-cuda:latest

build-cuda: 
	@docker build -t claughton/gromacs2018-cuda:$(VERSION) -f Dockerfile.cuda . \
	&& docker tag claughton/gromacs2018-cuda:$(VERSION) claughton/gromacs2018-cuda:latest

build: 
	@docker build -t claughton/gromacs2018:$(VERSION) . \
	&& docker tag claughton/gromacs2018:$(VERSION) claughton/gromacs2018:latest

publish: build
	@docker push claughton/gromacs2018:$(VERSION) \
	&& docker push claughton/gromacs2018:latest

publish-cuda: build-cuda
	@docker push claughton/gromacs2018-cuda:$(VERSION) \
	&& docker push claughton/gromacs2018-cuda:latest

.PHONY: all install uninstall build publish install-cuda uninstall-cuda build-cuda publish-cuda
