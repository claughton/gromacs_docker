#!/bin/sh
# Compile and run a short MD simulation on BPTI
gmx grompp -f mdrun.mdp -c bpti.gro -p bpti.top -o bpti-md.tpr
gmx mdrun -deffnm bpti-md
