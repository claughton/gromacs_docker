A Dockerized version of Gromacs 2018.
=====================================

This repo provides the functionality of Gromacs2018 via "shims". Instead of installing the software locally, you install
shims (short shell scripts) with the same names as the executables. When invoked, they in turn download(if required) and 
then launch a Docker container to actually execute the required command.

Installation:
-------------

The easy way:
```
% pip install git+https://bitbucket.org/claughton/gromacs_docker@<version>
```
where *<version>* is either "v2018" or "v2018-cuda", or "v2018-mpi" or "v2108-cuda-mpi",

or:
```
% git clone https://bitbucket.org/claughton/gromacs_docker.git
% cd gromacs_docker
% git checkout <version>
% make install
```

To remove:
```
% make uninstall
```

Usage:
------

The installation adds two scripts to /usr/local/bin (or wherever):

* `gmx`: provides a shim for the Gromacs 'gmx' command
* `gmx-select`: allows you to select which instruction set (sse2, avx_256, etc.) 
  is used. Check the top of the logfile from your first mdrun job to see what
  value is likely to give you the best performance, then type "gmx-select avx_256' or whatever.
  You should only need to set this once.

In the example folder is a short md job you can use to test your installation.

Rebuilding the Docker image:
----------------------------

If you want to build your own copy of the image (optional) you will need to
edit the Makefile to set your own Docker username, then for the standard versions:
```
% make build
% make publish
```

Acknowledgements
----------------

The Dockerfile is adapted from [Oliver Soellman's one](at https://github.com/soellman/gromacs)
The CLI is based on [Mike English's blog post](https://spin.atomicobject.com/2015/11/30/command-line-tools-docker/)
