#!/usr/bin/env bash
# configure the instruction set used by Gromacs. 

PROGNAME="$(basename $0)"
VERSION="v0.0.1"

# Helper functions for guards
error(){
  error_code=$1
  echo "ERROR: $2" >&2
  echo "($PROGNAME wrapper version: $VERSION, error code: $error_code )" >&2
  exit $1
}
check_cmd_in_path(){
  cmd=$1
  which $cmd > /dev/null 2>&1 || error 1 "$cmd not found!"
}

# Guards (checks for dependencies)
check_cmd_in_path docker
#check_cmd_in_path docker-machine
#docker-machine active > /dev/null 2>&1 || error 2 "No active docker-machine VM found."

is=$1
if [[ "$is" =~ ^(sse2|sse4.1|avx_128_fma|avx_256|avx2_256)$ ]]; then
  docker run  --rm -v gmx-vol:/opt/gromacs --entrypoint rm claughton/gmx2018 /opt/gromacs/default
  docker run  --rm -v gmx-vol:/opt/gromacs --entrypoint ln claughton/gmx2018 -s /opt/gromacs/"$is" /opt/gromacs/default
else
  error 3  "Unknown instruction set, options are: sse2 sse4.1 avx_128_fma avx_256 avx2_256."
fi
